import 'dart:convert';

import 'package:api_test/main.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class GetAPI extends StatefulWidget {
  const GetAPI({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<GetAPI> createState() => _GetAPIState();
}

class _GetAPIState extends State<GetAPI> {
  var jsonData;
  // print('sssss');
  Map<String, int> data = {};
  Future<void> _GetCovid19Today() async {
    var response = await http.get(
        Uri.parse("https://covid19.ddc.moph.go.th/api/Cases/today-cases-all"));
    jsonData = jsonDecode(utf8.decode(response.bodyBytes));
    print(response.body);

    data['new_case'] = jsonData['new_case'];
    data['total_case'] = jsonData['total_case'];
    data['new_case_excludeabroad'] = jsonData['new_case_excludeabroad'];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: FutureBuilder(
          future: _GetCovid19Today(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return Text('data');
              // return new Container(
              //   child: new Column(
              //     children: <Widget>[
              //       Text('เคสใหม่'),
              //       Text('${data['new_case']}'),
              //     ],
              //   ),
              // );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
      ),
    );
  }
}
